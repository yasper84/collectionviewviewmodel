//
// DemoCollectionViewController
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
//

import UIKit

final class DemoCollectionViewController: UIViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    private let viewModel = MyCollectionViewModel()
    
    private var timer: Timer?
    private func delayed(block: @escaping (() -> Void), timeDelay: TimeInterval = 3) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: timeDelay,
                                     repeats: false,
                                     block: { (_) in
                                        block()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Cycle through some example usages
        showMixedSection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    private func showMixedSection() {
        let collectionViewViewModel = CollectionViewViewModel()
        var section: CollectionViewSectionViewModel = CollectionViewSectionViewModel(header: viewModel.randomView)
        
        for pos in 1...30 {
            let stringRepresentation = String(format: "%d", pos)
            if arc4random()%2 == 1 {
                section.append(viewModel: stringRepresentation,
                               cellType: StringCollectionViewCell.self,
                               cellSize: StringCollectionViewCell.cellSize)
            } else {
                section.append(viewModel: MyCollectionViewCellViewModel(title: stringRepresentation,
                                                                        color: UIColor.blue),
                               cellType: ViewModelCollectionViewCell.self,
                               cellSize: StringCollectionViewCell.cellSize)
            }
        }
        
        collectionViewViewModel.append(section: section)
        collectionView.viewModel = collectionViewViewModel
        
        descriptionLabel.text = "CollectionView with multiple different cells"
        
        delayed(block: {
            self.showMultiSection()
        })
    }
    
    private func showMultiSection() {
        let collectionViewViewModel = CollectionViewViewModel()
        collectionViewViewModel.append(sections: viewModel.multiSectionViewModels(withHeaderFooter: true))
        collectionView.viewModel = collectionViewViewModel
        
        descriptionLabel.text = "CollectionView with multiple sections"
        
        delayed(block: {
            self.showOnClick()
        })
    }
    
    private func showOnClick() {
        let collectionViewViewModel = CollectionViewViewModel()
        
        let section = CollectionViewSectionViewModel(viewModels: [viewModel.randomCellViewModel,
                                                                  viewModel.randomCellViewModel,
                                                                  viewModel.randomCellViewModel],
                                                     cellType: ViewModelCollectionViewCell.self,
                                                     cellSize: StringCollectionViewCell.cellSize,
                                                     onCellClicked: { (viewModel, indexPath) in
                                                        print("Clicked on: \(viewModel.title) at \(indexPath.item)")
        },
                                                     insets: MyCollectionViewModel.defaultSectionInsets)
        collectionViewViewModel.append(section: section)
        collectionView.viewModel = collectionViewViewModel
        
        // Simulate some clicks
        delayed(block: {
            Array(0...2).forEach({ (rowPosition) in
                let selectIndexPath = IndexPath(item: rowPosition,
                                                section: 0)
                self.collectionView.delegate?.collectionView!(self.collectionView,
                                                              didSelectItemAt: selectIndexPath)
            })
            
            self.delayed(block: {
                self.showInsertRemove()
            })
        }, timeDelay: 1)
        
        descriptionLabel.text = "Callback for cells"
    }
    
    private func showInsertRemove() {
        let collectionViewModel = CollectionViewViewModel(animationType: GenCollectionViewFlowLayout.AnimationType.slideIn)
        collectionViewModel.append(sections: viewModel.multiSectionViewModels(withHeaderFooter: true))
        collectionView.viewModel = collectionViewModel
        
        // Apply append/remove
        delayed(block: {
            self.collectionView.viewModel?.remove(indexPath: IndexPath(item: 2,
                                                                       section: 1))
            self.delayed(block: {
                self.collectionView?.viewModel?.remove(indices: [IndexPath(item: 0, section: 0),
                                                                 IndexPath(item: 1, section: 1),
                                                                 IndexPath(item: 2, section: 2)])
                
                self.delayed(block: {
                    let insertCellConfiguration = CollectionViewCellConfiguration(cellViewModel: "1",
                                                                                  type: StringCollectionViewCell.self,
                                                                                  size: StringCollectionViewCell.cellSize)
                    self.collectionView?.viewModel?.append(cellConfigurator: insertCellConfiguration,
                                                           indexPath: IndexPath(item: 1,
                                                                                section: 2))
                    
                    self.delayed(block: {
                        self.collectionView.viewModel?.remove(sectionIndices: [1, 2])
                        
                        self.delayed(block: {
                            self.animatedFullReplaceSection()
                        }, timeDelay: 1)
                    }, timeDelay: 1)
                }, timeDelay: 1)
            }, timeDelay: 1)
        }, timeDelay: 1)
        
        descriptionLabel.text = "Insert/delete items from section"
    }
    
    private func animatedFullReplaceSection() {
        let firstSectionViewModel = CollectionViewSectionViewModel(viewModels: ["1", "2", "3"],
                                                                   cellType: StringCollectionViewCell.self,
                                                                   cellSize: StringCollectionViewCell.cellSize)
        let collectionViewModel = CollectionViewViewModel(sectionsViewModels: [firstSectionViewModel])
        collectionView.viewModel = collectionViewModel
        
        delayed(block: {
            let newSectionViewModel = CollectionViewSectionViewModel(viewModels: ["4", "1", "5", "6", "2", "7", "3"],
                                                                     cellType: StringCollectionViewCell.self,
                                                                     cellSize: StringCollectionViewCell.cellSize)
            collectionViewModel.replaceSection(newSectionViewModel: newSectionViewModel,
                                               sectionIndex: 0,
                                               animated: true)
            self.delayed(block: {
                let newSectionViewModel = CollectionViewSectionViewModel(viewModels: ["1", "5", "2", "7", "3"],
                                                                         cellType: StringCollectionViewCell.self,
                                                                         cellSize: StringCollectionViewCell.cellSize)
                collectionViewModel.replaceSection(newSectionViewModel: newSectionViewModel,
                                                   sectionIndex: 0,
                                                   animated: true)
                self.delayed(block: {
                    let newSectionViewModel = CollectionViewSectionViewModel(viewModels: ["1", "5"],
                                                                             cellType: StringCollectionViewCell.self,
                                                                             cellSize: StringCollectionViewCell.cellSize)
                    collectionViewModel.replaceSection(newSectionViewModel: newSectionViewModel,
                                                       sectionIndex: 0,
                                                       animated: true)
                    self.delayed(block: {
                        let newSectionViewModel = CollectionViewSectionViewModel(viewModels: ["1", "2", "3", "4", "5"],
                                                                                 cellType: StringCollectionViewCell.self,
                                                                                 cellSize: StringCollectionViewCell.cellSize)
                        collectionViewModel.replaceSection(newSectionViewModel: newSectionViewModel,
                                                           sectionIndex: 0,
                                                           animated: true)
                    }, timeDelay: 1)
                }, timeDelay: 1)
            }, timeDelay: 1)
        }, timeDelay: 1)
        
        descriptionLabel.text = "Replace entire section animated"
    }
}
