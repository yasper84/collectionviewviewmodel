//
// ViewModelCollectionViewCell
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

final class ViewModelCollectionViewCell: UICollectionViewCell, ConfigurableCollectionViewCell {
    @IBOutlet private weak var colorView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!

    func configure(with viewModel: MyCollectionViewCellViewModel) {
        contentLabel.text = "CellViewModel: \(viewModel.title)"
        colorView.backgroundColor = viewModel.color
    }

    static var cellSize: CGSize {
        return CGSize(width: 100, height: 50)
    }
}
