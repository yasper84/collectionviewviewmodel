//
// StringCollectionViewCell
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

final class StringCollectionViewCell: UICollectionViewCell, ConfigurableCollectionViewCell {

    @IBOutlet private weak var colorView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!

    func configure(with viewModel: String) {
        contentLabel.text = "String: \(viewModel)"
        colorView.backgroundColor = UIColor.darkGray
    }

    static var cellSize: CGSize {
        return CGSize(width: 100, height: 50)
    }
}
