//
// MyCollectionViewCellViewModel
//
// Created by Jasper Siebelink on 21/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

final class MyCollectionViewCellViewModel: NSObject {
    let title: String
    let color: UIColor

    init(title: String, color: UIColor) {
        self.title = title
        self.color = color
    }
}
