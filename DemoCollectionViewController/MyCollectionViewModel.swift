//
// MyCollectionViewModel
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

final class MyCollectionViewModel: NSObject {
    static let defaultSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 20,
                                                                 left: 10,
                                                                 bottom: 0,
                                                                 right: 10)
    var randomView: UIView {
        let randomView = UIView()
        randomView.backgroundColor = UIColor.yellow
        randomView.frame = CGRect(x: 0,
                                  y: 0,
                                  width: UIScreen.main.bounds.size.width,
                                  height: 50)
        return randomView
    }

    var randomCellViewModel: MyCollectionViewCellViewModel {
        return MyCollectionViewCellViewModel(title: "\(arc4random()%100)", color: UIColor.blue)
    }

    var variedViewModels: [Any] {
        return ["Bla",
                randomCellViewModel,
                randomCellViewModel,
                "Bla2",
                randomCellViewModel]
    }

    func singleSection(withHeaderFooter: Bool = false) -> CollectionViewSectionViewModel {
        let headerView = randomView
        headerView.backgroundColor = UIColor.darkGray
        let footerView = randomView
        footerView.backgroundColor = UIColor.lightGray

        return CollectionViewSectionViewModel(header: headerView,
                                              viewModels: [randomCellViewModel,
                                                           randomCellViewModel,
                                                           randomCellViewModel],
                                              cellType: ViewModelCollectionViewCell.self,
                                              cellSize: ViewModelCollectionViewCell.cellSize,
                                              footer: footerView,
                                              insets: MyCollectionViewModel.defaultSectionInsets)
    }

    func multiSectionViewModels(withHeaderFooter: Bool = false) -> [CollectionViewSectionViewModel] {
        return [singleSection(withHeaderFooter: withHeaderFooter),
                singleSection(withHeaderFooter: withHeaderFooter),
                singleSection(withHeaderFooter: withHeaderFooter)]
    }
}
