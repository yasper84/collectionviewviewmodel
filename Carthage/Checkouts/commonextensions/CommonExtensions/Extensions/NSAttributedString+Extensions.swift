//
//  NSAttributedString+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension NSAttributedString {
    
    public static func textAttributes(font: UIFont = UIFont.systemFont(ofSize: 12),
                                      color: UIColor = UIColor.black,
                                      lineHeight: CGFloat = 1.0,
                                      lineSpacing: CGFloat = 1.0,
                                      alignment: NSTextAlignment) -> [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: color,
                NSAttributedString.Key.paragraphStyle: paragraphStyle(with: lineHeight,
                                                                      lineSpacing: lineSpacing,
                                                                      alignment: alignment)]
    }
    
    private static func paragraphStyle(with lineHeight: CGFloat,
                                       lineSpacing: CGFloat,
                                       alignment: NSTextAlignment) -> NSMutableParagraphStyle {
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeight
        return paragraphStyle
    }
}
