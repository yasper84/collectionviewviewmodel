//
//  CGPoint+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension CGPoint {
    private func CGPointDistanceSquared(from: CGPoint, to: CGPoint) -> CGFloat {
        return (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)
    }
    
    public func distance(to: CGPoint) -> CGFloat {
        return sqrt(CGPointDistanceSquared(from: self, to: to))
    }
    
    public func deltaPoint(otherPoint: CGPoint) -> CGPoint {
        return CGPoint(x: x - otherPoint.x,
                       y: y - otherPoint.y)
    }
}
