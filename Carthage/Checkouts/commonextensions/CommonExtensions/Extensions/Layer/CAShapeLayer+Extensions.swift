//
//  CAShapeLayer+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension CAShapeLayer {
    public func cutOut(pathToClip: UIBezierPath) {
        let newPath = UIBezierPath(rect: bounds)
        newPath.append(pathToClip)

        let clippedLayer = CAShapeLayer()
        clippedLayer.path = newPath.cgPath
        clippedLayer.fillRule = CAShapeLayerFillRule.evenOdd
        mask = clippedLayer
    }
}
