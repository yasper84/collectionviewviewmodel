//
//  UIView+Frame.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIView {

    public var top: CGFloat {
        return frame.origin.y
    }

    public var left: CGFloat {
        return frame.origin.x
    }

    public var bottom: CGFloat {
        return frame.origin.y + frame.size.height
    }

    public var right: CGFloat {
        return frame.origin.x + frame.width
    }

    public var height: CGFloat {
        get {
            return frame.size.height
        }
        set {
            var curSize = frame.size
            curSize.height = newValue
            frame.size = curSize
        }
    }

    public var width: CGFloat {
        get {
            return frame.size.width
        }
        set {
            var curSize = frame.size
            curSize.width = newValue
            frame.size = curSize
        }
    }

    public var absoluteCenter: CGPoint {
        return convert(center, to: nil)
    }
    
    public var originFrame: CGRect {
        return superview?.convert(frame, to: nil) ?? .zero
    }
}
