//
//  URL+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension URL {
    public func open() {
        UIApplication.shared.open(self,
                                  options: [:],
                                  completionHandler: nil)
    }
}
