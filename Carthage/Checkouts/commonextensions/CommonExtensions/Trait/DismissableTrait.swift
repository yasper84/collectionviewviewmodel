//
//  DismissableTrait.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

private struct Constants {
    static let buttonLeftOffset: CGFloat = 20
    static let buttonTopOffset: CGFloat = 5
    static let defaultButtonSize = CGSize(width: 40, height: 40)
}

public protocol DismissableTrait: class {
    func dismissPressed()
}

public extension DismissableTrait where Self: UIViewController {
    public func configureAsDismissable() {
        if navigationController != nil {
            return configureNavigationbarDismissable()
        } else {
            return configureModalDismissable()
        }
    }

    private func configureNavigationbarDismissable() {
        let cancelButton = UIBarButtonItem(image: #imageLiteral(resourceName: "button-cancel"),
                                           style: UIBarButtonItem.Style.plain,
                                           target: self,
                                           action: nil)
        navigationItem.leftBarButtonItem = cancelButton

        cancelButton.addClosure { [weak self] in
            self?.dismissPressed()
        }
    }

    private func configureModalDismissable() {
        let closeButton = UIButton(autoLayout: ())
        closeButton.setImage(#imageLiteral(resourceName: "button-cancel"), for: UIControl.State.normal)

        self.view.addSubview(closeButton)
        closeButton.pinToSuperviewTop(constant: Constants.buttonTopOffset,
                                      useSafeArea: true)
        closeButton.pinToSuperviewLeft(constant: Constants.buttonLeftOffset)
        closeButton.pinDimensions(size: Constants.defaultButtonSize)

        closeButton.addClosure { [weak self] in
            self?.dismissPressed()
        }
    }

    private func dismissPressed() {
        dismiss(animated: true, completion: nil)
    }
}
