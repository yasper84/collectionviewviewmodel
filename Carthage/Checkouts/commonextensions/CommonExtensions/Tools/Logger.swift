//
//  Logger
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
    
public enum LogLevel: Int {
    case verboseLevel
    case defaultLevel
    case errorLevel
    
    var icon: String {
        switch self {
        case .verboseLevel:
            return "😊"
        case .defaultLevel:
            return "✉️: "
        case .errorLevel:
            return "‼️: "
        }
    }
}
    
public func log(_ string: String,
         _ logLevel: LogLevel = .defaultLevel) {
    guard logLevel.rawValue >= logLevel.rawValue else { return }
    print("\(logLevel.icon)\(string)")
}

public func logError(_ string: String,
              origin: String = #function) {
    log("\(origin): \(string)", .errorLevel)
}
