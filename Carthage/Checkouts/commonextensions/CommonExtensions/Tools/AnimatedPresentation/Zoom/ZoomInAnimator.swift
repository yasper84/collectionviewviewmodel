//
//  ZoomInAnimator.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public final class ZoomInAnimator: AnimationAnimator {
    private lazy var animationImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let zoomOriginViewController: ZoomOriginViewControllerProtocol
    private var originalViewFrame: CGRect = .zero
    
    public init(zoomOriginViewController: ZoomOriginViewControllerProtocol) {
        self.zoomOriginViewController = zoomOriginViewController
    }
    
    public override func transitionDuration(using context: UIViewControllerContextTransitioning?) -> TimeInterval {
        return UINavigationController.Constants.customAnimationDuration
    }
    
    public override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if presenting {
            zoomIn(transitionContext)
        } else {
            zoomOut(transitionContext)
        }
    }
        
    private func zoomIn(_ transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to),
            let zoomOriginView = zoomOriginViewController.zoomInOriginView else {
                transitionContext.completeTransition(true)
                
                logError("Cannot find zoom origin during transition")
                return
        }
        
        // Configure and display the mimic
        originalViewFrame = zoomOriginView.originFrame
        animationImageView.frame = originalViewFrame
        animationImageView.image = zoomOriginViewController.animationImage
        animationImageView.layer.cornerRadius = zoomOriginView.layer.cornerRadius
        transitionContext.containerView.addSubview(animationImageView)
        
        // Hide the originView as
        zoomOriginView.isHidden = true

        // Animate the mimic into place
        let targetFrame = transitionContext.finalFrame(for: toViewController)
        let duration = transitionDuration(using: transitionContext)
        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: UIView.KeyframeAnimationOptions.calculationModeCubic,
                                animations:
            {
                // Hide the background over time and zoom in
                UIView.addKeyframe(withRelativeStartTime: 0,
                                   relativeDuration: 1,
                                   animations:
                    {
                        fromViewController.view.alpha = 0
                        self.animationImageView.frame = targetFrame

                })
        },
                                completion:
            { _ in
                // Hide the mimic and show the presented content
                self.animationImageView.isHidden = true
                fromViewController.view.alpha = 1
                transitionContext.containerView.addSubview(toViewController.view)
                transitionContext.completeTransition(true)
                
                self.zoomInEndFade(destinationViewController: toViewController)
        })
    }
    
    private func zoomInEndFade(destinationViewController: UIViewController) {
        let targetViewController = (destinationViewController as? UINavigationController)?.viewControllers.last ?? destinationViewController
        
        let viewsToFadeIn: [UIView] = targetViewController.view.subviews.filter({ !($0 is UIImageView) })
        viewsToFadeIn.forEach({ $0.alpha = 0 })
        
        UIView.animate(withDuration: 0.35,
                       animations: {
                        viewsToFadeIn.forEach({ $0.alpha = 1 })
        })
    }
    
    private func zoomOut(_ transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let zoomOriginView = zoomOriginViewController.zoomInOriginView else {
            transitionContext.completeTransition(true)
                
            logError("Cannot find zoom origin during transition")
            return
        }

        // Hide the presented content and show the minic
        fromViewController.view.isHidden = true
        animationImageView.isHidden = false
        
        // Animate into place
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       animations:
            {
                self.animationImageView.frame = self.originalViewFrame
            },
                       completion:
            { _ in
                // Hide the minic, clear animation vars & and show the original
                UIView.animate(withDuration: 0.3,
                               animations: {
                                zoomOriginView.isHidden = false
                                self.animationImageView.alpha = 0
                },
                               completion: { (_) in
                                self.animationImageView.removeFromSuperview()
                                transitionContext.completeTransition(true)
                                self.zoomOriginViewController.onZoomEnded()
                })
            })
    }
}

