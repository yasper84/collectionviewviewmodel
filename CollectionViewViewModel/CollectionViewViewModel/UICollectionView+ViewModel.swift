//
// UICollectionView+ViewModel
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
//

import UIKit

// Configurator meta
extension UICollectionView {
    private struct Constants {
        static var viewModelKey: UInt8 = 200
    }

    public var viewModel: CollectionViewViewModel? {
        get {
            return objc_getAssociatedObject(self, &Constants.viewModelKey) as? CollectionViewViewModel
        }
        set {
            if newValue == nil && viewModel != nil {
                delegate = nil
                dataSource = nil
                reloadData()
            }

            if let newViewModel = newValue {
                newViewModel.configure(collectionView: self)
            }

            objc_setAssociatedObject(self,
                                     &Constants.viewModelKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
