//
// CollectionViewCellConfiguration
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

// To be implemented by cells used in the configuration
public protocol ConfigurableCollectionViewCell where Self: UICollectionViewCell {
    associatedtype ViewModel: Equatable
    func configure(with model: ViewModel)
}

// Type Erasure
public protocol CollectionViewCellConfigurationProtocol {
    var cellType: UICollectionViewCell.Type { get }
    var size: CGSize? { get }
    
    func configure(_ cell: UICollectionViewCell)
    func cellClicked(indexPath: IndexPath)
    func equals(other: CollectionViewCellConfigurationProtocol) -> Bool
}

// Responsible for linking cell with cellviewmodel
public struct CollectionViewCellConfiguration<T: ConfigurableCollectionViewCell>: CollectionViewCellConfigurationProtocol {
    public typealias OnCellClicked = ((T.ViewModel, IndexPath) -> Void)
    
    public let cellViewModel: T.ViewModel?
    public let type: T.Type
    public let onCellClicked: OnCellClicked?
    public let size: CGSize?
    
    public init(cellViewModel: T.ViewModel?,
                type: T.Type,
                size: CGSize? = nil,
                onCellClicked: OnCellClicked? = nil) {
        self.cellViewModel = cellViewModel
        self.type = type
        self.onCellClicked = onCellClicked
        self.size = size
    }
    
    public var cellType: UICollectionViewCell.Type {
        return type
    }
    
    public func configure(_ cell: UICollectionViewCell) {
        guard let cellViewModel = cellViewModel else { return }
        (cell as? T)?.configure(with: cellViewModel)
    }
    
    public func cellClicked(indexPath: IndexPath) {
        guard let cellViewModel = cellViewModel else { return }
        onCellClicked?(cellViewModel, indexPath)
    }
    
    public func equals(other: CollectionViewCellConfigurationProtocol) -> Bool {
        guard let otherTyped = other as? CollectionViewCellConfiguration<T> else { return false}
        return self == otherTyped
    }
    
    public static func == (lhs: CollectionViewCellConfiguration<T>, rhs: CollectionViewCellConfiguration<T>) -> Bool {
        return lhs.cellViewModel == rhs.cellViewModel
    }
}
