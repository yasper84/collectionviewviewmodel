//
// CollectionViewViewModel+Mutation
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit
import CommonExtensions

// MARK: Section addition
extension CollectionViewViewModel {
    public func append(section: CollectionViewSectionViewModel,
                       animated: Bool = true) {
        registerCellsInSection(section)
        
        sectionsViewModels.append(section)
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.insertSections(IndexSet([sectionsViewModels.count-1]))
        }, completion: nil)
    }
    
    public func append(sections: [CollectionViewSectionViewModel],
                       animated: Bool = true) {
        let nextSectionPosition: Int = sectionsViewModels.count
        sections.forEach({
            append(section: $0,
                   animated: false)
        })
        
        guard animated else { return }
        let arrayToUpdate: [Int] = Array(nextSectionPosition...sectionsViewModels.count-1)
        collectionView?.performBatchUpdates({
            self.collectionView?.insertSections(IndexSet(arrayToUpdate))
        }, completion: nil)
    }
}

// MARK: Section removal
extension CollectionViewViewModel {
    public func remove(sectionIndex: Int,
                       animated: Bool = true) {
        sectionsViewModels.remove(at: sectionIndex)
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.deleteSections(IndexSet([sectionIndex]))
        }, completion: nil)
    }
    
    public func remove(sectionIndices: [Int],
                       animated: Bool = true) {
        let reverseSortedIndicesToRemove = sectionIndices.sorted().reversed()
        reverseSortedIndicesToRemove.forEach({
            remove(sectionIndex: $0,
                   animated: false)
        })
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.deleteSections(IndexSet(sectionIndices))
        }, completion: nil)
    }
}

// MARK: Section replace
extension CollectionViewViewModel {
    // Replace an entire section, optionally animated
    public func replaceSection(newSectionViewModel: CollectionViewSectionViewModel,
                               sectionIndex: Int = 0,
                               animated: Bool = false) {
        registerCellsInSection(newSectionViewModel)
        
        guard
            let collectionView = collectionView,
            sectionsViewModels.count > sectionIndex else
        {
            if sectionsViewModels.isEmpty {
                sectionsViewModels.append(newSectionViewModel)
            } else {
                let fallbackIndex: Int = sectionsViewModels.count - 1
                sectionsViewModels[fallbackIndex] = newSectionViewModel
            }
            self.collectionView?.reloadData()
            return
        }
        
        // Snapshot the old and new situation
        let existingItems = sectionsViewModels[sectionIndex].cellConfigurations
        let newItems = newSectionViewModel.cellConfigurations
        
        // Update the model by determining what to insert and delete
        sectionsViewModels[sectionIndex] = newSectionViewModel
        
        var rowsToInsert: [IndexPath] = []
        var rowsToDelete: [IndexPath] = []
        
        for (index, existingCellConfiguration) in existingItems.enumerated() {
            guard !newItems.contains(where: { $0.equals(other: existingCellConfiguration) }) else { continue }
            rowsToDelete.append(IndexPath(row: index,
                                          section: sectionIndex))
        }
        
        for (index, newCellConfiguration) in newItems.enumerated() {
            guard !existingItems.contains(where: { $0.equals(other: newCellConfiguration) }) else { continue }
            rowsToInsert.append(IndexPath(row: index,
                                          section: sectionIndex))
        }
        
        // Animate the new configuration
        collectionView.performBatchUpdates({
            collectionView.deleteItems(at: rowsToDelete)
            collectionView.insertItems(at: rowsToInsert)
        }, completion: nil)
    }
}

// MARK: Item addition, removal
extension CollectionViewViewModel {
    public func append<T: ConfigurableCollectionViewCell>(cellConfigurator: CollectionViewCellConfiguration<T>,
                                                          indexPath: IndexPath? = nil,
                                                          animated: Bool = true) {
        registerCellType(cellConfigurator.cellType)
        
        let rowIndexPath: IndexPath
        if let indexPath = indexPath {
            rowIndexPath = indexPath
        } else {
            let sectionPosition: Int = sectionsViewModels.isEmpty ? 0 : sectionsViewModels.count - 1
            let newRowPosition: Int = sectionsViewModels.isEmpty ? 0 : sectionsViewModels[sectionPosition].cellConfigurations.count
            rowIndexPath = IndexPath(item: newRowPosition,
                                     section: sectionPosition)
        }
        
        var sectionViewModel = sectionsViewModels[rowIndexPath.section]
        sectionViewModel.cellConfigurations.insert(cellConfigurator,
                                                   at: rowIndexPath.item)
        sectionsViewModels[rowIndexPath.section] = sectionViewModel
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.insertItems(at: [rowIndexPath])
        }, completion: nil)
    }
    
    public func append<T: ConfigurableCollectionViewCell>(cellConfigurators: [CollectionViewCellConfiguration<T>],
                                                          indices: [IndexPath],
                                                          animated: Bool = true) {
        cellConfigurators.forEach({ registerCellType($0.cellType) })
        
        for (index, currentCellConfigurator) in cellConfigurators.enumerated() {
            let indexPath: IndexPath = indices[index]
            
            var sectionViewModel = sectionsViewModels[indexPath.section]
            sectionViewModel.cellConfigurations.insert(currentCellConfigurator,
                                                       at: indexPath.item)
            sectionsViewModels[indexPath.section] = sectionViewModel
        }
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.insertItems(at: indices)
        }, completion: nil)
    }
    
    public func remove(indexPath: IndexPath,
                       animated: Bool = true) {
        var sectionViewModel: CollectionViewSectionViewModel = sectionsViewModels[indexPath.section]
        sectionViewModel.cellConfigurations.remove(at: indexPath.item)
        sectionsViewModels[indexPath.section] = sectionViewModel
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.deleteItems(at: [indexPath])
        }, completion: nil)
    }
    
    public func remove(indices: [IndexPath],
                       animated: Bool = true) {
        indices.forEach({ remove(indexPath: $0,
                                 animated: false) })
        
        guard animated else { return }
        collectionView?.performBatchUpdates({
            self.collectionView?.deleteItems(at: indices)
        }, completion: nil)
    }
}

// MARK: Registration support
private extension CollectionViewViewModel {
    func registerCellsInSection(_ section: CollectionViewSectionViewModel) {
        section.cellConfigurations.forEach({ registerCellType($0.cellType) })
    }
    
    func registerCellType(_ cellType: UICollectionViewCell.Type) {
        collectionView?.registerCell(type: cellType)
    }
}
