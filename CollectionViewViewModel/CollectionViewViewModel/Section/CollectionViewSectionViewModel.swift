//
// CollectionViewSectionViewModel
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

public struct CollectionViewSectionViewModel {
    public let insets: UIEdgeInsets?
    public let header: UIView?
    public var cellConfigurations: [CollectionViewCellConfigurationProtocol] = []
    public let footer: UIView?

    public init(header: UIView? = nil,
                cellConfigurations: [CollectionViewCellConfigurationProtocol],
                footer: UIView? = nil,
                insets: UIEdgeInsets? = nil) {
        self.insets = insets
        self.header = header
        self.cellConfigurations = cellConfigurations
        self.footer = footer
    }

    public init<T: ConfigurableCollectionViewCell>(header: UIView? = nil,
                                                   viewModels: [T.ViewModel],
                                                   cellType: T.Type,
                                                   cellSize: CGSize? = nil,
                                                   onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil,
                                                   footer: UIView? = nil,
                                                   insets: UIEdgeInsets? = nil) {
        self.insets = insets
        self.header = header
        self.footer = footer

        cellConfigurations = viewModels.map({ (viewModel) -> CollectionViewCellConfigurationProtocol in
            return CollectionViewCellConfiguration<T>(cellViewModel: viewModel,
                                                      type: cellType,
                                                      size: cellSize,
                                                      onCellClicked: onCellClicked)
        })
    }

    public init(header: UIView? = nil,
                footer: UIView? = nil,
                insets: UIEdgeInsets? = nil) {
        self.insets = insets
        self.header = header
        self.footer = footer
        cellConfigurations = []
    }

    public mutating func append<T: ConfigurableCollectionViewCell>(viewModel: T.ViewModel? = nil,
                                                                   cellType: T.Type,
                                                                   cellSize: CGSize? = nil,
                                                                   onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil) {
        let newConfiguration = CollectionViewCellConfiguration<T>(cellViewModel: viewModel,
                                                                  type: cellType,
                                                                  size: cellSize,
                                                                  onCellClicked: onCellClicked)
        cellConfigurations.append(newConfiguration)
    }
}
