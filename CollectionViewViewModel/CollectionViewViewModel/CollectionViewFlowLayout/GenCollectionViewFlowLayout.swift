//
// GenCollectionViewFlowLayout
//
// Created by Jasper Siebelink on 21/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

public final class GenCollectionViewFlowLayout: UICollectionViewFlowLayout {
    public struct Constants {
        static let initialScale: CGFloat = 0.1
        public static let defaultAnimation = AnimationType.scaleFade
    }

    public enum AnimationType {
        case scale
        case fade
        case scaleFade
        case slideIn
    }

    private let animationType: AnimationType
    private var insertingIndexPaths = [IndexPath]()
    private var insertingSections = [Int]()

    private var deletingIndexPaths = [IndexPath]()
    private var deletingSections = [Int]()

    public required init(animationType: AnimationType) {
        self.animationType = animationType
        super.init()
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        self.animationType = Constants.defaultAnimation
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        scrollDirection = UICollectionView.ScrollDirection.vertical
        itemSize = CGSize.zero
    }

    override public func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)

        reset()
        updateItems.forEach { (item) in
            if item.updateAction == UICollectionViewUpdateItem.Action.insert {
                guard let indexPath = item.indexPathAfterUpdate else { return }

                if indexPath.row == INT64_MAX {
                    insertingSections.append(indexPath.section)
                } else {
                    insertingIndexPaths.append(indexPath)
                }
            } else if item.updateAction == UICollectionViewUpdateItem.Action.delete {
                guard let indexPath = item.indexPathBeforeUpdate else { return }

                if indexPath.row == INT64_MAX {
                    deletingSections.append(indexPath.section)
                } else {
                    deletingIndexPaths.append(indexPath)
                }
            }
        }
    }

    override public func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates()
        reset()
    }

    override public func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)

        if insertingIndexPaths.contains(itemIndexPath) || insertingSections.contains(itemIndexPath.section) {
            if [AnimationType.scaleFade,
                AnimationType.fade,
                AnimationType.slideIn].contains(animationType) {
                attributes?.alpha = 0.0

                if
                    animationType == AnimationType.slideIn,
                    let collectionView = collectionView
                {
                    attributes?.transform = CGAffineTransform(translationX: 0,
                                                              y: collectionView.frame.size.height/2)
                }
            }

            if [AnimationType.scaleFade,
                AnimationType.scale].contains(animationType) {
                attributes?.transform = CGAffineTransform(scaleX: Constants.initialScale,
                                                          y: Constants.initialScale)
            }
        }

        return attributes
    }

    override public func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath)

        if deletingIndexPaths.contains(itemIndexPath) || deletingSections.contains(itemIndexPath.section) {
            if [AnimationType.scaleFade,
                AnimationType.fade,
                AnimationType.slideIn].contains(animationType) {
                attributes?.alpha = 0.0
            }

            if [AnimationType.scaleFade,
                AnimationType.scale].contains(animationType) {
                attributes?.transform = CGAffineTransform(scaleX: Constants.initialScale,
                                                          y: Constants.initialScale)
            }
        }

        return attributes
    }

    private func reset() {
        insertingIndexPaths.removeAll()
        insertingSections.removeAll()
        deletingIndexPaths.removeAll()
        deletingSections.removeAll()
    }
}
