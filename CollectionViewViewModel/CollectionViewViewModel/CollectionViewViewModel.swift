//
// CollectionViewViewModel
//
// Created by Jasper Siebelink on 20/02/2019
// Copyright Jasper Siebelink. All rights reserved.
// 

import UIKit

open class CollectionViewViewModel: NSObject {
    
    public var sectionsViewModels: [CollectionViewSectionViewModel] = []
    public weak var collectionView: UICollectionView?
    public var flowLayout: GenCollectionViewFlowLayout {
        didSet {
            collectionView?.collectionViewLayout = flowLayout
        }
    }
    
    public init(sectionsViewModels: [CollectionViewSectionViewModel] = [],
                animationType: GenCollectionViewFlowLayout.AnimationType = GenCollectionViewFlowLayout.Constants.defaultAnimation) {
        self.sectionsViewModels = sectionsViewModels
        flowLayout = GenCollectionViewFlowLayout(animationType: animationType)
        super.init()
    }
    
    open func configure(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        // If this instance is set to the CollectionView after the SectionViewModels were added,
        // the cells won't be registered yet
        sectionsViewModels.forEach { (sectionViewModel) in
            sectionViewModel.cellConfigurations.forEach({ (cellConfiguration) in
                collectionView.registerCell(type: cellConfiguration.cellType)
            })
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.collectionViewLayout = flowLayout
        
        collectionView.reloadData()
    }
    
    open func reset() {
        sectionsViewModels.removeAll()
    }
    
    open func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        collectionView?.endEditing(true)
    }
}

extension CollectionViewViewModel: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionsViewModels.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               numberOfItemsInSection section: Int) -> Int {
        return sectionsViewModels[section].cellConfigurations.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellConfiguration: CollectionViewCellConfigurationProtocol = sectionsViewModels[indexPath.section].cellConfigurations[indexPath.item]
        let className = cellConfiguration.cellType.className
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: className,
                                                      for: indexPath)
        cellConfiguration.configure(cell)
        return cell
    }
}

extension CollectionViewViewModel: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView,
                               didSelectItemAt indexPath: IndexPath) {
        sectionsViewModels[indexPath.section].cellConfigurations[indexPath.item].cellClicked(indexPath: indexPath)
    }
}

extension CollectionViewViewModel: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        if flowLayout.itemSize != CGSize.zero {
            return flowLayout.itemSize
        } else {
            return sectionsViewModels[indexPath.section].cellConfigurations[indexPath.item].size ?? UICollectionViewFlowLayout.automaticSize
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        return
            sectionsViewModels[section].insets ??
                (collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ??
                UIEdgeInsets.zero
    }
}
