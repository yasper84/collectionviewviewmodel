//
//  CollectionViewViewModel.h
//  CollectionViewViewModel
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CollectionViewViewModel.
FOUNDATION_EXPORT double CollectionViewViewModelVersionNumber;

//! Project version string for CollectionViewViewModel.
FOUNDATION_EXPORT const unsigned char CollectionViewViewModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CollectionViewViewModel/PublicHeader.h>


